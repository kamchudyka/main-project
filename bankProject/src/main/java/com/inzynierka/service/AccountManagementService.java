package com.inzynierka.service;

import com.inzynierka.core.model.PrimaryAccount;
import com.inzynierka.core.model.PrimaryTransaction;
import com.inzynierka.core.model.SavingsAccount;
import com.inzynierka.core.model.SavingsTransaction;
import com.inzynierka.core.model.User;
import com.inzynierka.repository.PrimaryAccountRepository;
import com.inzynierka.repository.SavingsAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.Date;
import java.util.Objects;
import java.util.Random;

@Service
public class AccountManagementService {

    @Autowired
    private PrimaryAccountRepository primaryAccountRepository;

    @Autowired
    private SavingsAccountRepository savingsAccountRepository;

    @Autowired
    private UserCreationService userCreationService;

    @Autowired
    private TransactionService transactionService;

    public PrimaryAccount createPrimaryAccount() {
        PrimaryAccount primaryAccount = new PrimaryAccount();
        primaryAccount.setAccountBalance(new BigDecimal(0.0));
        primaryAccount.setAccountNumber(primaryAccountGen());

        primaryAccountRepository.save(primaryAccount);

        return primaryAccountRepository.findByAccountNumber(primaryAccount.getAccountNumber());
    }

    public SavingsAccount createSavingsAccount() {
        SavingsAccount savingsAccount = new SavingsAccount();
        savingsAccount.setAccountBalance(new BigDecimal(0.0));
        savingsAccount.setAccountNumber(savingsAccountGen());

        savingsAccountRepository.save(savingsAccount);

        return savingsAccountRepository.findByAccountNumber(savingsAccount.getAccountNumber());
    }

    public void deposit(String accountType, double amount, Principal principal) {
        User user = userCreationService.findByUsername(principal.getName());
        Date date = new Date();

        if (accountType.equalsIgnoreCase("Konto główne")) {
            depositAndSaveTransactionForPrimaryAccount(amount, user, date);

        } else {
            depositAndSaveTransactionForSavingsAccount(amount, user, date);
        }
    }

    private void depositAndSaveTransactionForSavingsAccount(double amount, User user, Date date) {
        SavingsAccount savingsAccount = user.getSavingsAccount();
        savingsAccount.setAccountBalance(savingsAccount.getAccountBalance().add(new BigDecimal(amount)));
        savingsAccountRepository.save(savingsAccount);

        SavingsTransaction savingsTransaction = new SavingsTransaction(date, "Przelew na konto oszczędnościowe", "Inny", "Ukończony", amount, savingsAccount.getAccountBalance(), savingsAccount);
        transactionService.saveSavingsDepositTransaction(savingsTransaction);
    }

    private void depositAndSaveTransactionForPrimaryAccount(double amount, User user, Date date) {
        PrimaryAccount primaryAccount = user.getPrimaryAccount();
        primaryAccount.setAccountBalance(primaryAccount.getAccountBalance().add(new BigDecimal(amount)));
        primaryAccountRepository.save(primaryAccount);


        PrimaryTransaction primaryTransaction = new PrimaryTransaction(date, "Przelew na konto główne", "Inny", "Ukończony", amount, primaryAccount.getAccountBalance(), primaryAccount);
        transactionService.savePrimaryDepositTransaction(primaryTransaction);
    }

    public void withdraw(String accountType, double amount, Principal principal) {
        User user = userCreationService.findByUsername(principal.getName());

        if (accountType.equalsIgnoreCase("Konto główne")) {
            withdrawAndSaveTransactionForPrimaryAccount(amount, user);
        } else {
            withdrawAndSaveTransactionForSavingsAccount(amount, user);
        }
    }

    private void withdrawAndSaveTransactionForSavingsAccount(double amount, User user) {
        SavingsAccount savingsAccount = user.getSavingsAccount();
        savingsAccount.setAccountBalance(savingsAccount.getAccountBalance().subtract(new BigDecimal(amount)));
        savingsAccountRepository.save(savingsAccount);

        Date date = new Date();
        SavingsTransaction savingsTransaction = new SavingsTransaction(date, "Wypłać z konta oszczędnościowego", "Inny", "Ukończony", amount, savingsAccount.getAccountBalance(), savingsAccount);
        transactionService.saveSavingsWithdrawTransaction(savingsTransaction);
    }

    private void withdrawAndSaveTransactionForPrimaryAccount(double amount, User user) {
        PrimaryAccount primaryAccount = user.getPrimaryAccount();
        primaryAccount.setAccountBalance(primaryAccount.getAccountBalance().subtract(new BigDecimal(amount)));
        primaryAccountRepository.save(primaryAccount);

        Date date = new Date();

        PrimaryTransaction primaryTransaction = new PrimaryTransaction(date, "Wypłać z konta głównego", "Account", "Ukończony", amount, primaryAccount.getAccountBalance(), primaryAccount);
        transactionService.savePrimaryWithdrawTransaction(primaryTransaction);
    }

    private int primaryAccountGen() {
        int firstRandom = getRandomInt();

        PrimaryAccount foundAccount = primaryAccountRepository.findByAccountNumber(firstRandom);
        if (Objects.isNull(foundAccount)) {
            return firstRandom;
        }

        int temp = 0;

        while (Objects.nonNull(foundAccount)) {
            temp = getRandomInt();
            foundAccount = primaryAccountRepository.findByAccountNumber(temp);
        }

        return temp;

    }

    private int savingsAccountGen() {
        int firstRandom = getRandomInt();

        SavingsAccount foundAccount = savingsAccountRepository.findByAccountNumber(firstRandom);
        if (Objects.isNull(foundAccount)) {
            return firstRandom;
        }

        int temp = 0;

        while (Objects.nonNull(foundAccount)) {
            temp = getRandomInt();
            foundAccount = savingsAccountRepository.findByAccountNumber(temp);
        }

        return temp;

    }

    private int getRandomInt() {
        Random random = new Random();
        return random.nextInt(1000);
    }
}
