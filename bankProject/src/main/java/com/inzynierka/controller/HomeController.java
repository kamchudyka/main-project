package com.inzynierka.controller;

import java.security.Principal;

import com.inzynierka.service.UserCreationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.inzynierka.repository.RoleRepository;
import com.inzynierka.core.model.PrimaryAccount;
import com.inzynierka.core.model.SavingsAccount;
import com.inzynierka.core.model.User;

@Controller
public class HomeController {
	
	@Autowired
	private UserCreationService userCreationService;
	
	@Autowired
    private RoleRepository roleRepository;
	
	@RequestMapping("/")
	public String home() {
		return "redirect:/index";
	}
	
	@RequestMapping("/index")
    public String index() {
        return "index";
    }
	
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String signup(Model model) {
        User user = new User();

        model.addAttribute("user", user);

        return "signup";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String signupPost(@ModelAttribute("user") User user, Model model) {

        if(userCreationService.checkUserExists(user.getUsername(), user.getEmail()))  {

            if (userCreationService.checkEmailExists(user.getEmail())) {
                model.addAttribute("emailExists", true);
            }

            if (userCreationService.checkUsernameExists(user.getUsername())) {
                model.addAttribute("usernameExists", true);
            }

            return "signup";
        } else {

            userCreationService.createUser(user);

            return "redirect:/";
        }
    }

	@RequestMapping("/mainPage")
	public String mainPage(Principal principal, Model model) {
        User user = userCreationService.findByUsername(principal.getName());
        PrimaryAccount primaryAccount = user.getPrimaryAccount();
        SavingsAccount savingsAccount = user.getSavingsAccount();

        model.addAttribute("primaryAccount", primaryAccount);
        model.addAttribute("savingsAccount", savingsAccount);

        return "mainPage";
    }
}
