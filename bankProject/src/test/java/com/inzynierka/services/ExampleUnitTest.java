package com.inzynierka.services;

import com.inzynierka.core.model.User;
import com.inzynierka.repository.UserRepository;
import com.inzynierka.service.SecurityService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ExampleUnitTest {

    private static final String USER_NAME = "testUser";

    @Mock
    UserRepository userRepository;

    @Mock
    User user;

    SecurityService securityService;

    @Before
    public void setUp(){
        securityService = new SecurityService();

        ReflectionTestUtils.setField(securityService, null, userRepository, UserRepository.class);

        when(userRepository.findByUsername(any())).thenReturn(user);
        when(user.getUsername()).thenReturn(USER_NAME);
    }

    @Test
    public void shouldFindCreatedUser(){
        //given

        //when
        UserDetails foundUser = securityService.loadUserByUsername(USER_NAME);

        //then
        assertTrue(foundUser.getUsername().equals(USER_NAME));

    }
}
