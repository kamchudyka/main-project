package com.inzynierka.services;

import com.inzynierka.InzynierkaApplicationTests;
import com.inzynierka.core.model.User;
import com.inzynierka.core.model.security.Role;
import com.inzynierka.repository.RoleRepository;
import com.inzynierka.service.AccountManagementService;
import com.inzynierka.service.UserCreationService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@Transactional
public class AccountManagementServiceTest extends InzynierkaApplicationTests{

    private static final int TRANSFER_AMOUNT = 1000;

    @Autowired
    AccountManagementService accountManagementService;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserCreationService userCreationService;

    @Mock
    Principal principal;

    private User savedUser;

    private User userToBeSaved;

    @Before
    public void setUp(){
        saveUserRole();
        userToBeSaved = buildMockUser();
        when(principal.getName()).thenReturn("user");
    }

    @Test
    public void shouldDepositMoneyToPrimaryAccountForPreviouslyCreatedUser() {
        //given
        savedUser = userCreationService.createUser(userToBeSaved);

        //when
        accountManagementService.deposit("Konto główne", TRANSFER_AMOUNT, principal);

        //then
        assertEquals(savedUser.getPrimaryAccount().getAccountBalance().intValue(), TRANSFER_AMOUNT);
    }

    @Test
    public void shouldWithdrawMoneyFromSavingsAccountForPreviouslyCreatedUser(){
        //given
        savedUser = userCreationService.createUser(userToBeSaved);

        //when
        accountManagementService.withdraw("Konto oszczędnościowe", TRANSFER_AMOUNT, principal);

        //then
        assertEquals(savedUser.getSavingsAccount().getAccountBalance().intValue(), -TRANSFER_AMOUNT);
    }

    private void saveUserRole(){
        Role role = new Role.RoleBuilder()
                .withRoleId(1)
                .withName("USER")
                .build();
        roleRepository.saveAndFlush(role);
    }
    private User buildMockUser(){
        return new User.Builder()
                .withFirstName("firstName")
                .withLastName("lastName")
                .withPassword("password")
                .withEmail("email")
                .withUserName("user")
                .withPhone("555-555-555")
                .build();
    }
}
