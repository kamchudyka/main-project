package com.inzynierka.services;

import com.inzynierka.InzynierkaApplicationTests;
import com.inzynierka.core.model.PrimaryAccount;
import com.inzynierka.core.model.SavingsAccount;
import com.inzynierka.core.model.User;
import com.inzynierka.core.model.security.Role;
import com.inzynierka.repository.PrimaryAccountRepository;
import com.inzynierka.repository.RoleRepository;
import com.inzynierka.repository.SavingsAccountRepository;
import com.inzynierka.service.TransactionService;
import com.inzynierka.service.UserCreationService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

@Transactional
public class TransactionServiceTest extends InzynierkaApplicationTests {

    private static final String USER_NAME = "user";
    private static final String ACCOUNT_BALANCE = "1000";
    private static final String TRANSFER_AMOUNT = "1000";
    private static final String SAVINGS_ACCOUNT = "Konto oszczędnościowe";
    private static final String PRIMARY_ACCOUNT = "Konto główne";

    private PrimaryAccount savedPrimaryAccount;

    private SavingsAccount savedSavingsAccount;

    private User user;

    @Autowired
    PrimaryAccountRepository primaryAccountRepository;

    @Autowired
    SavingsAccountRepository savingsAccountRepository;

    @Autowired
    TransactionService transactionService;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserCreationService userCreationService;

    @Before
    public void setUp() {
        savedPrimaryAccount = buildAndSavePrimaryAccount();
        savedSavingsAccount = buildAndSaveSavingsAccount();
    }

    @Test
    public void shouldTransferMoneyFromPrimaryToSavingsAccount() throws Exception {
        //given

        //when
        transactionService.betweenAccountsTransfer(PRIMARY_ACCOUNT, SAVINGS_ACCOUNT, TRANSFER_AMOUNT, savedPrimaryAccount, savedSavingsAccount);

        //then
        String expectedAccountBalance = sumStringValues(TRANSFER_AMOUNT, ACCOUNT_BALANCE);

        assertEquals(savedPrimaryAccount.getAccountBalance(), new BigDecimal(0));
        assertEquals(savedSavingsAccount.getAccountBalance(), new BigDecimal(expectedAccountBalance));
    }

    @Test
    public void shouldTransferMoneyFromSavingsToPrimaryAccount() throws Exception {
        //given

        //when
        transactionService.betweenAccountsTransfer(SAVINGS_ACCOUNT, PRIMARY_ACCOUNT, TRANSFER_AMOUNT, savedPrimaryAccount, savedSavingsAccount);

        //then
        String expectedAccountBalance = sumStringValues(TRANSFER_AMOUNT, ACCOUNT_BALANCE);

        assertEquals(savedPrimaryAccount.getAccountBalance(), new BigDecimal(expectedAccountBalance));
        assertEquals(savedSavingsAccount.getAccountBalance(), new BigDecimal(0));
    }

    @Test
    public void shouldTransferMoneyFromPrimaryAccountToAnotherUserInBank() {
        //given
        saveUserRole();
        user = buildMockUser();
        user = userCreationService.createUser(user);

        //when
        transactionService.toSomeoneElseTransfer(user, PRIMARY_ACCOUNT, TRANSFER_AMOUNT, savedPrimaryAccount, savedSavingsAccount);

        //then
        String expectedAccountBalance = substractStringValues(TRANSFER_AMOUNT, ACCOUNT_BALANCE);

        assertEquals(savedPrimaryAccount.getAccountBalance().intValue(), Integer.parseInt(expectedAccountBalance));
        assertEquals(user.getPrimaryAccount().getAccountBalance().intValue(), Integer.parseInt(TRANSFER_AMOUNT));
    }

    //Dopisanie tego samego tylko ze z savings

    private PrimaryAccount buildAndSavePrimaryAccount() {
        PrimaryAccount primaryAccount = new PrimaryAccount.PrimaryAccountBuilder()
                .withAccountBalance(new BigDecimal(ACCOUNT_BALANCE))
                .withAccountNumber(111)
                .build();

        return primaryAccountRepository.saveAndFlush(primaryAccount);
    }

    private SavingsAccount buildAndSaveSavingsAccount() {
        SavingsAccount savingsAccount = new SavingsAccount.SavingsAccountBuilder()
                .withAccountBalance(new BigDecimal(ACCOUNT_BALANCE))
                .withAccountNumber(222)
                .build();

        return savingsAccountRepository.saveAndFlush(savingsAccount);
    }

    private String sumStringValues(String firstValue, String secondValue) {
        int summedUp = Integer.parseInt(firstValue) + Integer.parseInt(secondValue);

        return Integer.toString(summedUp);
    }

    private String substractStringValues(String firstValue, String secondValue){
        int substracted = Integer.parseInt(firstValue) - Integer.parseInt(secondValue);

        return Integer.toString(substracted);
    }

    private void saveUserRole(){
        Role role = new Role.RoleBuilder()
                .withRoleId(1)
                .withName("USER")
                .build();
        roleRepository.save(role);
    }

    private User buildMockUser(){
        return new User.Builder()
                .withFirstName("firstName")
                .withLastName("lastName")
                .withPassword("password")
                .withEmail("email")
                .withUserName(USER_NAME)
                .withPhone("555-555-555")
                .build();
    }
}
