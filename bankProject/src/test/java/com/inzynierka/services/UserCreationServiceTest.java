package com.inzynierka.services;

import com.inzynierka.InzynierkaApplicationTests;
import com.inzynierka.core.model.User;
import com.inzynierka.core.model.security.Role;
import com.inzynierka.repository.RoleRepository;
import com.inzynierka.repository.UserRepository;
import com.inzynierka.service.UserCreationService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

@Transactional
public class UserCreationServiceTest extends InzynierkaApplicationTests {

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserCreationService userCreationService;

    @Autowired
    UserRepository userRepository;

    @Before
    public void setUp(){
        saveUserRole();
    }

    @Test
    public void shouldCreateUserViaServiceMethod(){
        //given
        User user = buildMockUser();

        //when
        userCreationService.createUser(user);

        //then
        User createdUser = userRepository.findByUsername(user.getUsername());

        assertThat(createdUser.getUsername(), equalTo(user.getUsername()));
    }

    private void saveUserRole(){
        Role role = new Role.RoleBuilder()
                .withRoleId(1)
                .withName("USER")
                .build();
        roleRepository.saveAndFlush(role);
    }
    private User buildMockUser(){
        return new User.Builder()
                .withFirstName("firstName")
                .withLastName("lastName")
                .withPassword("password")
                .withEmail("testEmail")
                .withUserName("testUser")
                .withPhone("555-555-552")
                .build();
    }
}
