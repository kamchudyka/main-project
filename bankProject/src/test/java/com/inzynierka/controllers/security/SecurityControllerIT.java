package com.inzynierka.controllers.security;

import com.inzynierka.controller.HomeController;
import com.inzynierka.controllers.WebAppIT;
import com.inzynierka.core.model.PrimaryAccount;
import com.inzynierka.core.model.SavingsAccount;
import com.inzynierka.core.model.User;
import com.inzynierka.service.UserCreationService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.transaction.annotation.Transactional;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
public class SecurityControllerIT extends WebAppIT {

    @Autowired
    HomeController homeController;

    @Autowired
    UserCreationService userCreationService;

    @Mock
    UserCreationService userCreationServiceMock;

    @Mock
    PrimaryAccount primaryAccountMock;

    @Mock
    SavingsAccount savingsAccountMock;

    @Mock
    User mockUser;

    private ResultActions act;

    @Before
    public void setUp() {
        initMocks(this);
        ReflectionTestUtils.setField(homeController, null, userCreationServiceMock, UserCreationService.class);
    }

    @After
    public void onExit() {
        ReflectionTestUtils.setField(homeController, null, userCreationService, UserCreationService.class);
    }

    @Test
    public void shouldDoNotGetAccessToSeeContentOfMainPageBeforeLogIn() throws Exception {
        //given

        //when
        userTriesToLoadMainPageOfApplicationBeforeLogIn();

        //then should be redirected to index page
    }

    @Test
    @WithMockUser(roles = "USER")
    public void shouldGetAccessToSeeContentOfMainPageAfterLogIn() throws Exception {
        //given
        prepareMocks();

        //when
        userTriesToLoadMainPageOfApplicationAfterLogIn();

        //then should be redirected to main page
        resultStatusIs(HttpStatus.OK);

    }

    private void prepareMocks() {
        when(userCreationServiceMock.findByUsername(anyString())).thenReturn(mockUser);
        when(mockUser.getPrimaryAccount()).thenReturn(primaryAccountMock);
        when(mockUser.getSavingsAccount()).thenReturn(savingsAccountMock);
    }

    private void userTriesToLoadMainPageOfApplicationAfterLogIn() throws Exception {
        act = mvcWithAuthentication.perform(get("/mainPage"))
                .andDo(print());
    }


    private void userTriesToLoadMainPageOfApplicationBeforeLogIn() throws Exception {
        act = mvcWithAuthentication.perform(get("/mainPage"))
                .andExpect(redirectedUrl("http://localhost/index"))
                .andDo(print());
    }


    private void resultStatusIs(HttpStatus expectedStatus) throws Exception {
        act.andExpect(status().is(expectedStatus.value()));
    }
}
