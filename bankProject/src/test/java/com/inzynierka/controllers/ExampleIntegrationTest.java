package com.inzynierka.controllers;

import com.inzynierka.controller.TransferController;
import com.inzynierka.core.model.User;
import com.inzynierka.core.model.security.Role;
import com.inzynierka.repository.RoleRepository;
import com.inzynierka.repository.UserRepository;
import com.inzynierka.service.UserCreationService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.math.BigDecimal;
import java.security.Principal;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@Transactional
public class ExampleIntegrationTest extends WebAppIT {

    private static final String PRIMARY_ACCOUNT = "Konto główne";
    private static final int TRANSFER_AMOUNT = 10000;


    @Autowired
    TransferController transferController;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserCreationService userCreationService;

    @Autowired
    UserRepository userRepository;

    @Mock
    Principal principal;

    private User sender;
    private User receiver;

    @Before
    public void setUp() {
        when(principal.getName()).thenReturn("sender");

        saveUserRole();
        sender = builderMockUser("sender", "sender@gmail.com");
        receiver = builderMockUser("receiver", "receiver@gmail.com");

        sender = userCreationService.createUser(sender);
        receiver = userCreationService.createUser(receiver);
    }

    @Test
    public void shouldTransferMoneyToAnotherUser() throws Exception {
        //given

        //when
        userTriesTo(receiver.getUsername(), PRIMARY_ACCOUNT, TRANSFER_AMOUNT);

        //then
        User foundReceiver = userRepository.findByUsername(receiver.getUsername());
        assertTrue(foundReceiver.getPrimaryAccount().getAccountBalance().equals(new BigDecimal(TRANSFER_AMOUNT)));
    }

    private void userTriesTo(String userName, String accuntType, int amount) throws Exception {
        MultiValueMap<String, String> multiValueMap = new LinkedMultiValueMap<>();

        multiValueMap.add("recipientName", userName);
        multiValueMap.add("accountType", accuntType);
        multiValueMap.add("amount", Integer.toString(amount));


        mvc.perform(post("/transfer/toSomeoneElse")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .principal(principal)
                .params(multiValueMap))
                .andDo(print());
    }

    private void saveUserRole() {
        Role role = new Role.RoleBuilder()
                .withRoleId(1)
                .withName("USER")
                .build();
        roleRepository.save(role);
    }

    private User builderMockUser(String userName, String email) {
        return new User.Builder()
                .withFirstName("firstName")
                .withLastName("lastName")
                .withPassword("password")
                .withEmail(email)
                .withUserName(userName)
                .withPhone("555-555-555")
                .build();
    }
}
