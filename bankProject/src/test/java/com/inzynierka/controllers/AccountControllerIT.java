package com.inzynierka.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.inzynierka.core.model.User;
import com.inzynierka.core.model.security.Role;
import com.inzynierka.repository.RoleRepository;
import com.inzynierka.service.UserCreationService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.security.Principal;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@Transactional
public class AccountControllerIT extends WebAppIT {

    private static final String USER_NAME = "user";
    private static final int TRANSFER_AMOUNT = 10000;
    private static final String PRIMARY_ACCOUNT = "Konto główne";
    private static final String DEPOSIT = "deposit";
    private static final String WITHDRAW = "withdraw";

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserCreationService userCreationService;

    @Mock
    Principal principal;

    private User user;

    @Before
    public void setUp(){
        saveUserRole();
        user = buildMockUser();
        user = userCreationService.createUser(user);

        when(principal.getName()).thenReturn(USER_NAME);
    }

    @Test
    @WithMockUser(roles = "USER")
    public void shouldDepositMoneyIntoPrimaryAccountForPreviouslyCreatedUser() throws Exception{
        //given

        //when
        userTriesTo(TRANSFER_AMOUNT, PRIMARY_ACCOUNT, DEPOSIT);

        //then
        assertEquals(user.getPrimaryAccount().getAccountBalance().intValue(), TRANSFER_AMOUNT);
    }

    @Test
    @WithMockUser(roles = "USER")
    public void shouldWithdrawMoneyFromPrimaryAccountForPreviouslyCreatedUser() throws Exception{
        //given

        //when
        userTriesTo(TRANSFER_AMOUNT, PRIMARY_ACCOUNT, WITHDRAW);

        //then
        assertEquals(user.getPrimaryAccount().getAccountBalance().intValue(), -TRANSFER_AMOUNT);
    }

    private void saveUserRole(){
        Role role = new Role.RoleBuilder()
                .withRoleId(1)
                .withName("USER")
                .build();
        roleRepository.save(role);
    }

    private User buildMockUser(){
        return new User.Builder()
                .withFirstName("firstName")
                .withLastName("lastName")
                .withPassword("password")
                .withEmail("email")
                .withUserName(USER_NAME)
                .withPhone("555-555-555")
                .build();
    }

    private void userTriesTo(int amount, String accuntType, String operation) throws Exception {
        MultiValueMap<String, String> multiValueMap = new LinkedMultiValueMap<>();

        multiValueMap.add("amount", Integer.toString(amount));
        multiValueMap.add("accountType", accuntType);


        mvcWithAuthentication.perform(post("/account/" + operation)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .params(multiValueMap))
                .andDo(print());
    }

}
